﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using NAND_Prog;

namespace ID_Register
{
  
    [Export(typeof(Register)),
        ExportMetadata("Name", "ID_Register")]
  
    public class ID_Register : Register
    {

        [ImportMany("ID Register Operation", typeof(Operation))]       //Набір операцій для цього регістра , імпортується з DLL чіпа в якому він використовується
        public override List<Operation> operations { get { return _operations; } set { _operations = value; } }


        //[ImportingConstructor]
        //public ID_Register([Import("ID Register name", typeof(string))]string Name,
        //                   [Import("ID Register size", typeof(int))]int Size)
        //{
        //    name = Name;                                                 // імя і розмір цього регістра  імпортується з DLL чіпа в якому він використовується
        //    size = Size;
        //}

    

        [Import("ID Register Interpreted", typeof(Func<Register, string>), AllowDefault = true)]// Інтерпретація цього регістра , імпортується з DLL чіпа в якому він використовується
        public override Func<Register, string> Interpreted { get; set; }
        
    }
}
